#!/usr/bin/bash

# ./Pipeline6.3.1.sh --help
G='\033[0;32m' ; C='\033[0;36m' ; NC='\033[0m'
ksize=25 ; thread=8 ; maxd=64 ; setnumb=10 ; setnumbb=50 ; maxcross=2 ; edit=2 ; maxrep=8 ; remN=False ; mau=0 ; prefix="probe"
echo "" ; echo -e "${C}Customized options :${NC}"
for arg in "$@" ; do
   shift
   case "$arg" in
      --target) target=$1        ; echo -e "${C}     - Fichier de séquences cibles :" $1 ${NC}
      ;;
      --nontarget) nontarget=$1  ; echo -e "${C}     - Fichier de séquences non cibles :" $1 ${NC} 
      ;;
      --kmersize) ksize=$1       ; echo -e "${C}     - Taille des sondes demandée :" $1 ${NC}
      ;;
      --thread) thread=$1        ; echo -e "${C}     - Nombre de CPU utilisé par jellyfish et cd-hit :" $1 ${NC}
      ;;
      --maxdegen) maxd=$1        ; echo -e "${C}     - Score de dégenerescence maximum accepté :" $1 ${NC}
      ;;
      --setnumb) setnumb=$1      ; echo -e "${C}     - Nombre de sets minimaux de sondes produits :" $1 ${NC}
      ;;
      --setnumblong) setnumbb=$1 ; echo -e "${C}     - Nombre de sets minimaux produit lors du second design (pour les extrémités)" $1 ${NC}
      ;;
      --maxcross) maxcross=$1    ; echo -e "${C}     - Nombre d'hybridation croisée maximum :" $1 ${NC}
      ;;
      --edit) edit=$1            ; echo -e "${C}     - Distance d'édition :" $1 ${NC}
      ;;
      --maxrep) maxrep=$1        ; echo -e "${C}     - Maximum de répétition d'une base :" $1 ${NC}
      ;;
      --remN) remN=$1            ; echo -e "${C}     - Suppression des séquences avec des N dans le fichier d'entrée :" $1 ${NC}
      ;;
      --mau) mau=$1              ; echo -e "${C}     - Nombre de match maximum des kmers cibles contre les kmers non cibles :" $1 ${NC}
      ;;
      --prefix) prefix=$1        ; echo -e "${C}     - Préfixe utilisé pour renommer les sondes à la fin de l'analyse :" $1 ${NC}
      ;;
      --help) echo "usage : ./Pipeline.sh --target FILE --nontarget FILE [OPTIONS]"; \
              echo " "
              echo "--target	Target file (FASTA) [mandatory option]"; \
              echo "--nontarget	Non target file (FASTA) [mandatory option]"; \
              echo "--kmersize	Define probe length for design [default 25]"; \
              echo "--thread	Number of CP to use [default 8]"; \
              echo "--maxdegen	Maximum of allowed probe degenerescence [default 64]"; \
              echo "--maxcross	Maximum of allowed cross-hybridization [default 2]"; \
              echo "--setnumb	Number of minimal probes sets [default 10]"; \
              echo "--setnumblong	Number of minimal probes sets for second design on N-term"; \
              echo "--edit	Setup the edit distance"; \
              echo "--maxrep	Number of allowed nucleotide repeat [default 8]"; \
              echo "--remN	Remove sequences with N from input file [default False] [inactive option]"; \
              echo "--mau	Maximum of allowed match of kmers from target to kmers from nontarget [default 0]"; \
              echo " "; \                     
              echo "--help	Show this help message"; \
              exit
      ;;
   esac 2>> /dev/null
done

#Si ces répertoires existent, je delete car leurs contenus s'accumulent
rm all_res.txt tmp_blast.fas 2>> /dev/null ; rm -r temp/ 2>> /dev/null

#Début réel du script
echo "" ; echo -e "${G}INFOS : 0%. Production des sets minimaux${NC}" ; echo ""
time python3 Kaspod6.3.py -k $ksize -t $thread -md $maxd -ns $setnumb -mc $maxcross -e $edit --max_repet $maxrep -mau $mau --target=$target --nontarget=$nontarget 2>> LogFile

echo "" ; echo -e "${G}INFOS : 25%. Première sélection de sondes${NC}" ; echo ""
time ./Epuisement_patman.sh 2>> LogFile

echo "" ; echo -e "${G}INFOS : 50%. Détermination des potentielles séquences longues partiellement couvertes et stockage des résultats du premier design${NC}" ; echo ""
time python3 Kaspod6.1.1_part2.py $target $nontarget $prefix $setnumbb $thread $edit $maxcross $maxd $maxrep $mau $ksize 2>> LogFile

touch redesign
go=$(cat redesign | wc -l) 2>> LogFile
b=0
if [ "$go" == "$b" ]
then
echo "" ; echo -e "${G}INFOS : 100%. Les séquences sont couvertes avec homogénéité, pas de second design, fin du programme${NC}" ; echo ""
fi
if [ "$go" -gt "$b" ]
then

echo "" ; echo -e "${G}INFOS : 75%. Sélection de sondes supplémentaires pour les séquences longues${NC}" ; echo ""
time ./bash_2.sh 2>> LogFile

echo "" ; echo -e "${G}INFOS : 95%. Création du fichier final de résulats${NC}" ; echo ""
time python3 Kaspod6.1.1_part3.py $target $nontarget $prefix 2>> LogFile 

echo "" ; echo -e "${G}INFOS : 100%. Programme terminé${NC}" ; echo ""
fi
rm -r temp/ 2>> /dev/null ; rm tmp_blast.fas res_spetmp* redesign res.txt speci_temp sondes_temp* all_res* Jeu_de_sonde* nb sondes.txt 2>> /dev/null
