#!/usr/bin/python

#Récupération des informations sur les sondes poduites pour les portons des séquences longues non couvertes
#Ajout de ces informations au fichier final de résultats

from os import listdir
from collections import defaultdict
import gzip
import os
import sys

target         = sys.argv[1]
nontarget      = sys.argv[2]
prefix         = sys.argv[3]
sondes_select  = list()
liste_cible    = set()
sondes_seq     = dict()
score          = dict()
sensi          = dict()
match_sen      = defaultdict(list)
speci          = defaultdict(int)
deb            = defaultdict(lambda: defaultdict(int))

with open('nb','r') as f1 :
	for l in f1 : 
		nb_seq = l.strip()
os.system('rm nb')		
with open ("Jeu_de_sonde_final2","r") as f1 :
	for l1 in f1 :
		l1 = l1.strip()
		sondes_select.append(l1)
with open ("tmp_blast.fas","r") as f2 :
	for l2 in f2 :
		save = False
		l2 = l2.strip()
		if l2.startswith('>') :
			nom = l2.lstrip('>')
		else :
			sondes_seq[nom] = l2

degen = dict(A=1,C=1,G=1,T=1,M=2,R=2,W=2,S=2,Y=2,K=2,V=3,H=3,D=3,B=3,I=4,N=4)
for sonde in sondes_select : 
	score[sonde] = 1
	for nt in sondes_seq[sonde] : 
		score[sonde] = score[sonde] * degen[nt]
os.system("touch speci_temp")
with open ("speci_temp","r") as f1 : 
	for l in f1 : 
		l = l.strip().split('\t')
		speci[l[0]] = int(l[1])
with open ("all_res.txt", "r") as f1 : 
	for l in f1 : 
		l = l.strip().split('\t')             	#On enlève les sauts de ligne à la fin des lignes (uniquement pratique)
		match_sen[int(l[1])].append(l[0])
		liste_cible.add(l[0])
		deb[l[1]][l[0]]   = int(l[2])           	#Récupération de la position de début du match dans le fichier
for k in sorted(match_sen.keys()) :           #Pour chaque valeur (ID de sondes) dans le dictionnaire remplit juste avant :
	t = set(match_sen[k])   	#t prend la valeur du dict pour la sonde courante (k) transformée en liste parsée par les ## (liste de nom de target)
	sensi[str(k)] = round((len(t)/int(nb_seq)*100) , 2)		#On attribue une valeur de sensibilité à chaque sondes (k) 
os.system("rm all_res.txt")
with open ("tempo_bash_tab", "a") as out : 
	for sonde in set(sondes_select) : 
		leset = set(match_sen[int(sonde)])
		leset = list(leset)
		leset.sort()
		print(prefix+"_"+sonde, sondes_seq[sonde], deb[str(sonde)][list(deb[str(sonde)])[-1]], sensi[sonde], speci[sonde], score[sonde], leset , sep='\t', file=out)

os.system("cat tempo_bash_tab | sort -k1 -n | uniq > Results.tab")
print("Fin de la deuxième sélection de sondes")
#La position sur a séquence la plus longue est une valeur vide
os.system("rm Jeu_de_sonde_final Jeu_de_sonde_final2 Jeu_de_sondes_select redesign res_sen.txt res.txt sondes2.txt sondes.txt speci_temp tempo_bash_tab temporary_results5 temporary_results6 tmp_blast.fas all_res3.gz ; rm -r temp/")
