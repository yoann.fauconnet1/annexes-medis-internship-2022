#!/usr/bin/bash

#Traitement des sets minimaux de la seconde sélection de sondes : Réduction du fichier patman et sélection
echo "Log for second probe selection" >> /dev/stderr
while read line
 do
  awk -v line=$line -F'\t' 'BEGIN{OFS="\t"} $2==line{print $1,$2,$3}' <(zcat res_sen.txta.gz) | sort | uniq | gzip >> res_sen.txt.gz &
 done < sondes2.txt
wait < <(jobs -p) 
rm res_sen.txta.gz

zcat res_sen.txt.gz | tr ';' '#' | tr ' ' '#' | tr '|' '#' | tr ',' '#' | gzip > all_res2.gz
mv all_res2.gz res_sen.txt.gz

temp=$(zcat res_sen.txt.gz | wc -l)
while [ $temp -gt 0 ]
do

nb_seq=$(zcat res_sen.txt.gz \
       | cut -f1 \
       | sort | uniq \
       |wc -l)
export best_probe=$(zcat res_sen.txt.gz \
	   | cut -f1,2 \
	   | sort -k1 | uniq \
	   | cut -f2 \
	   | sort | uniq -c \
	   | awk -F'\t' -v nb_seq=$nb_seq -F' ' '{print $2 "\t" $1/nb_seq*100}' | sort -n -r -k2 | head -1 | cut -f1)
zcat res_sen.txt.gz | awk -F'\t' -v best_probe=$best_probe '$2 == best_probe {print $0}' | cut -f1 | sort | uniq > match_probe_temp	   
	   
echo $nb_seq séquences cibles et la sonde courante est la n°$best_probe
echo $best_probe >> Jeu_de_sondes_select

touch probe_to_delete_temp
parallel -k -a match_probe_temp "zcat res_sen.txt.gz | awk -F'\t' -v mpt={} -v bp=\$best_probe '\$1==mpt && \$2!=bp {print \$2}' | sort -n | uniq > probe_temp_{} ; pos_best=\$(zcat res_sen.txt.gz | awk -F'\t' -v mpt={} -v best_probe=\$best_probe '\$1==mpt && \$2==best_probe {print \$3}' | head -1) ; window_start=\$(echo \$pos_best-400 | bc) ; window_end=\$(echo \$pos_best+400 | bc) ;while read line ; do
                                                               pos_probe=\$(zcat res_sen.txt.gz | awk -F'\t' -v mpt={} -v pt=\$line '\$1==mpt && \$2==pt {print \$3}' | head -1) 
                                                               if [[ \$pos_probe -gt \$window_start && \$pos_probe -lt \$window_end ]]
                                                               	then
                                                               	 echo {}','\$line >> probe_to_delete_temp_{}
                                                               fi
                                                               done < probe_temp_{} 
                                                               echo {}','\$best_probe >> probe_to_delete_temp_{}
                                                               echo '+1 analysed target' >> /dev/stderr"
cat probe_to_delete_tem* > probe_to_delete_temp  

echo "End of probe selection in $best_probe window" 
while IFS=, read cible sonde 
do
echo 'Removing probe' $sonde 'from' $cible 'in patman_file' > /dev/stderr
zcat res_sen.txt.gz | awk -F'\t' -v sonde=$sonde -v cible=$cible '$2==sonde&&$1==cible{print $0}'  >> line_to_delete_temp
done < probe_to_delete_temp 

cat line_to_delete_temp | sort | uniq > line_del
comm -23 <(zcat res_sen.txt.gz| sort) <(cat line_del | sort) | gzip > temp5
mv temp5 res_sen.txt.gz
temp=$(zcat res_sen.txt.gz | wc -l)
echo "Probe selected"
rm probe_to_delete_temp* probe_temp* line_del line_to_delete_temp match_probe_temp
done

cat Jeu_de_sondes_select | sort -n | uniq > Jeu_de_sonde_final2
echo "Result file production" > /dev/stderr
rm res_sen.txt.gz
