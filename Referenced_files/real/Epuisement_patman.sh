#!/usr/bin/bash

#Création d'un fichier patman plus petit ne contenant que les lignes des sondes des sets minimaux
#Traitement du fichier patman et sélection du premier jeu de sondes
echo "Log for first probe selection" >> /dev/stderr
while read line
 do
  awk -v line=$line -F'\t' 'BEGIN{OFS="\t"} $2==line{print $1,$2,$3}' all_res.txt | sort | uniq | gzip >> tempo &
 done < sondes.txt
wait < <(jobs -p)

cp tempo all_res3.gz
zcat tempo | tr ';' '#' | tr ' ' '#' | tr '|' '#' | tr ',' '#' | gzip > all_res2.gz
mv all_res2.gz tempo

temp=$(zcat tempo | wc -l)
while [ $temp -gt 0 ]
do

nb_seq=$(zcat tempo \
       | cut -f1 \
       | sort | uniq \
       |wc -l)
echo $nb_seq "cibles restantes"
export best_probe=$(zcat tempo \
	   | cut -f1,2 \
	   | sort -k1 | uniq \
	   | cut -f2 \
	   | sort | uniq -c \
	   | awk -F'\t' -v nb_seq=$nb_seq -F' ' '{print $2 "\t" $1/nb_seq*100}' | sort -n -r -k2 | head -1 | cut -f1)
zcat tempo | awk -F'\t' -v best_probe=$best_probe '$2 == best_probe {print $0}' | cut -f1 | sort | uniq > match_probe_temp	   

echo $nb_seq "target remaining and current probe is n°"$best_probe
echo $best_probe >> Jeu_de_sondes_select

#Il n'arrive pas à ouvrir les probe_temp_ dans le script, pourquoi ? Peut-être que le problème vient de la création du fichier match_probe_temp qui donne naissance à tous les autres dérivés ou à un défaut de best_probe. On arrête le script ici, on regarde le premier match_probe_temp et best_probe pour voir si tout va bien. Une théorie autre serait que le problème apparaisse à la fin, quand tempo (fichier résultat de patman est presque vide. Auquel cas je n'en comprendrais pas la cause.
#Il est aussi complexe de dire si c'est le design 1 ou 2 dans le premier log qui pose problème, ce qui demande de faire les tests script par script comme je te l'ai proposé.

#echo $best_probe
#exit

touch probe_to_delete_temp
parallel -k -a match_probe_temp "zcat tempo | awk -F'\t' -v mpt={} -v bp=\$best_probe '\$1==mpt && \$2!=bp {print \$2}' | sort -n | uniq > probe_temp_{} ; pos_best=\$(zcat tempo | awk -F'\t' -v mpt={} -v best_probe=\$best_probe '\$1==mpt && \$2==best_probe {print \$3}' | head -1) ; window_start=\$(echo \$pos_best-400 | bc) ; window_end=\$(echo \$pos_best+400 | bc) ;while read line ; do
                                                               pos_probe=\$(zcat tempo | awk -F'\t' -v mpt={} -v pt=\$line '\$1==mpt && \$2==pt {print \$3}' | head -1) 
                                                               if [[ \$pos_probe -gt \$window_start && \$pos_probe -lt \$window_end ]]
                                                               	then
                                                               	 echo {}','\$line >> probe_to_delete_temp_{}
                                                               fi
                                                               done < probe_temp_{} 
                                                               echo {}','\$best_probe >> probe_to_delete_temp_{}
                                                               echo '+1 analysed target' >> /dev/stderr"
cat probe_to_delete_tem* > probe_to_delete_temp  

echo "End of probe selection in $best_probe window"
while IFS=, read cible sonde 
do
echo 'Removing probe' $sonde 'from' $cible 'in patman_file' >> /dev/stderr

zcat tempo | awk -F'\t' -v sonde=$sonde -v cible=$cible '$2==sonde&&$1==cible{print $0}'  >> line_to_delete_temp
done < probe_to_delete_temp 

cat line_to_delete_temp | sort | uniq > line_del
comm -23 <(zcat tempo| sort) <(cat line_del | sort) | gzip > temp5
mv temp5 tempo
temp=$(zcat tempo | wc -l)
echo "Probe selected"
rm probe_to_delete_temp* probe_temp* line_del line_to_delete_temp match_probe_temp
done

cat Jeu_de_sondes_select | sort -n | uniq > Jeu_de_sonde_final
rm tempo
