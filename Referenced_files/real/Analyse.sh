#!/bin/bash

#Pipeline d'analyse des résultats de KASpOD.py
#En premier argument : fichier de résultat | En second argument : fichier de séquences cibles
#Usage example : time ./Pipeline6.3.sh --target CAH_short.fasta --nontarget FDH_short.fasta ; ./Analyse.sh Results.tab CAH_short.fasta

Results=${1}
target=${2}
cat ${Results} | grep -v 'ID' | grep -v 'target' | cut -f1,2 > sondestab.csv
python3 csv_to_fasta.py sondestab.csv 
revseq sample.fasta sss
cat sss >> sample.fasta
rm sss
exit
python3 fasta_length.py ${2} > target_seq_len

java -jar ../../RDPTools/ProbeMatch.jar sample.fasta ${target} -n 2 -o ${target}.coverage

cat ${target}'.coverage' | sort +0d -1 +3n -4 | sed 1d |awk '{ print $3,$1 }' | nl -w2 -s'>' |awk '{print $2, $1}'| nawk '{t[$1]?t[$1]=t[$1]";"$2:t[$1]=$2}END{for (i in t){print i,t[i]}}'|awk '{gsub(/[0-9]+>/, "", $2)}1' | sort -k1 > ${target}'.coverage_synthetized_by_sequence' 

cat ${target}'.coverage' | sed 1d |awk '{ print $4,$1 }' | sort -k1n | nl -w2 -s'>' |awk '{print $2, $1}'| nawk '{t[$1]?t[$1]=t[$1]";"$2:t[$1]=$2}END{for (i in t){print i,t[i]}}'|awk '{gsub(/[0-9]+>/, "", $2)}1' | sort -k1 > ${target}'.coverage_synthetized_by_sequence2'

cat ${target}'.coverage' | sed 1d |awk '{ print $5,$1 }' | nl -w2 -s'>' |awk '{print $2, $1}'| nawk '{t[$1]?t[$1]=t[$1]";"$2:t[$1]=$2}END{for (i in t){print i,t[i]}}'|awk '{gsub(/[0-9]+>/, "", $2)}1' | sort -k1 > ${target}'.coverage_synthetized_by_sequence3'

join ${target}'.coverage_synthetized_by_sequence' ${target}'.coverage_synthetized_by_sequence2' | join - ${target}'.coverage_synthetized_by_sequence3' | sort -k1 > ${target}'.coverage_synthetized_by_sequence_f'

join ${target}'.coverage_synthetized_by_sequence_f' <(cat target_seq_len | sort -k1) > ${target}'.coverage_synthetized_by_sequence_fi'

rm sondestab.csv sample.fasta target_seq_len
mkdir Analyse.sh_results
mv *.coverage* Analyse.sh_results/
