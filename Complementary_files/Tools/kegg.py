#Télécharger des séquences de Kegg depuis un fichier contenant les identifiants des gènes (ligne par ligne)

'''
python3 kegg.py fichier_liste_d_IDs

How it works :
ma_liste = ["aaa:Acav_0635", "aaa:Acav_3056"]
seq = Bio.KEGG.REST.kegg_get(ma_liste, option="ntseq").read()
print(seq)

'''

from os import listdir
import Bio.KEGG.REST
import time
import sys
import os

#On récuère le fichier input en argument + Définition de la date du jour
input = sys.argv[1]
current_date = time.strftime("%Y%m%d")


#Création d'un répertoire temp et parsing du fichier d'input en fichiers de 10 lignes :
os.system("mkdir temp ; split "+input+" -d -l 10 temp/tmp_file_")

#Travail sur les fichiers successivement :
fichiers = listdir('temp')
for fichier in fichiers :
	ma_liste = list()
	with open ('temp/'+fichier,'r') as f1 :
		for l in f1 :
			l = l.strip()
			ma_liste.append(l)
	seq = Bio.KEGG.REST.kegg_get(ma_liste, option="ntseq").read()
	#On concatène toutes les séquences obtenues dans ce fichier de résultats
	with open (input+'_seq'+current_date+'.fasta', 'a') as out :
		print(seq, file=out)

os.system("rm -r temp/")

