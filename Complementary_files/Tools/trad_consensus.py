#!/usr/bin/python3 

import re   
IUB = dict(A='A', C='C', G='G', T='T', M='AC', R='AG', W='AT', S='CG', Y='CT', K='GT', V='ACG', H='ACT', D='AGT', B='CGT', I='ACGT', N='ACGT')
sequences = set()
propre = set()

	
with open ('temp/tmp_blast_00', 'r') as f1 : 
	for l in f1 : 
		l = l.strip()
		if not l.startswith('>') :
			print("next seq AATCGATGC")
			sequences.add(l)
			false_seq = l
			while re.search('M|R|W|S|Y|K|V|H|D|B|I|N', false_seq) != None : 
				for sequence in sequences : #Séquence est un str, élement de a qui est un set
					for i in range(len(sequence)) : 
						false_seq = false_seq[i+1:]
						print(false_seq)
						for nt in range(len(IUB[sequence[i]])) : 
							d = sequence.replace(sequence[i], IUB[sequence[i]][nt]) 
							print(d)
							propre.add(d)
				for v in propre : 
					sequences.add(v)					

#Séquences est un set donc on ne retrouvera pas de séquences en doubles, par contre, il faut éliminer toutes celles qui contiennent des symboles du code iupac pour ne garder que des séquences nucléotidiques traitables par les algorithmes utilisés. 															
for v in sequences :
	if not re.search('M|R|W|S|Y|K|V|H|D|B|I|N', v) :
		print(v)


'''
with open ('temp/tmp_blast_00', 'r') as f1 : 
	for l in f1 : 
		l = l.strip()
		if not l.startswith('>') :
			for nt in IUB :
				for variant in range(len(IUB[nt])) : 
					print(IUB[nt][variant])
					li = l.replace(nt, IUB[nt][variant])
					sequences.add(li)

for v in sequences :
	if not re.search('M|R|W|S|Y|K|V|H|D|B|I|N', v) :
		print(v)
'''					

