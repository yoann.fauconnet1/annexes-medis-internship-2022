python3 csv_to_fasta.py sondestab.csv

java -jar RDPTools/ProbeMatch.jar sample.fasta Cible_MIDORI.fasta -n 2 -o Cible_MIDORI.fasta.coverage

cat 'Cible_MIDORI.fasta.coverage' | sort +0d -1 +3n -4 | sed 1d |awk '{ print $3,$1 }' | nl -w2 -s'>' |awk '{print $2, $1}'| nawk '{t[$1]?t[$1]=t[$1]";"$2:t[$1]=$2}END{for (i in t){print i,t[i]}}'|awk '{gsub(/[0-9]+>/, "", $2)}1' | sort -k1 > 'Cible_MIDORI.fasta.coverage_synthetized_by_sequence' 

cat 'Cible_MIDORI.fasta.coverage' | sed 1d |awk '{ print $4,$1 }' | sort -k1n | nl -w2 -s'>' |awk '{print $2, $1}'| nawk '{t[$1]?t[$1]=t[$1]";"$2:t[$1]=$2}END{for (i in t){print i,t[i]}}'|awk '{gsub(/[0-9]+>/, "", $2)}1' | sort -k1 > 'Cible_MIDORI.fasta.coverage_synthetized_by_sequence2'

cat 'Cible_MIDORI.fasta.coverage' | sed 1d |awk '{ print $5,$1 }' | nl -w2 -s'>' |awk '{print $2, $1}'| nawk '{t[$1]?t[$1]=t[$1]";"$2:t[$1]=$2}END{for (i in t){print i,t[i]}}'|awk '{gsub(/[0-9]+>/, "", $2)}1' | sort -k1 > 'Cible_MIDORI.fasta.coverage_synthetized_by_sequence3'

join 'Cible_MIDORI.fasta.coverage_synthetized_by_sequence' 'Cible_MIDORI.fasta.coverage_synthetized_by_sequence2' | join - 'Cible_MIDORI.fasta.coverage_synthetized_by_sequence3' | sort -k1 > Cible_MIDORI.fasta.coverage_synthetized_by_sequence_f

join Cible_MIDORI.fasta.coverage_synthetized_by_sequence_f <(cat ../MIDORI_seqlen.csv | sort -k1) > Cible_MIDORI.fasta.coverage_synthetized_by_sequence_fi
