#!/usr/bin/python

#KASpOD -- A k-mer based algorithm for high-specific oligonucleotide design.
#Copyright (C) 2012 Nicolas Parisot
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#Contact: g2im.kaspod@gmail.com

from Bio.Seq import Seq					#To work with fasta files with efficiency
from Bio import motifs					#Clusters alignment and consensus sequences
from Bio import AlignIO					#Clusters alignment and consensus sequences
from Bio.Align import AlignInfo				#Clusters alignment and consensus sequences
from Bio import SeqIO
from Bio import SearchIO
from collections import defaultdict			#Dictionaries with default values to ease computation
from os import listdir					#To interact with files and directories on the system
import argparse  					#Command line argument parser
import sys       					#Allow interaction with linux system
import os	 					#Allow bash command in a python script
import re						#For regular expression

pclust              = 0.88    		#Similarity threshold for clusters
c                   = 0			
scoretot            = 0
compt_id            = 0
clust               = False	
Not_Found           = True
cluster_id          = ""
nomseqAsc           = list()
all_names           = set()
liste_min_probes    = set()
seq_with_cross      = set()
seq_all_probes      = set()
candidate_probe     = set()
Jeu_de_sondes_final = set()
h_header            = dict()
id_seq              = dict()
deb                 = defaultdict(lambda: defaultdict(int))
ID_pos              = dict()
nom_degen           = dict()
linknames           = dict()
nom_seq             = dict()
Each_best_probes    = defaultdict(list)
sensi               = defaultdict(int)
speci               = defaultdict(int)
match_spe           = defaultdict(str)
match_sen           = defaultdict(str)
All_infos           = defaultdict(list)	

#Score de dégenerescence établit à partir de la table IUB (Code IUPAC)
degen = dict(A=1,C=1,G=1,T=1,M=2,R=2,W=2,S=2,Y=2,K=2,V=3,H=3,D=3,B=3,I=4,N=4)

#Command line argument parsing to create script options             
pa = argparse.ArgumentParser(description='KASpOD -- A k-mer based algorithm for high-specific oligonucleotide design. Version 0.3 (09/2012). Feel free to contact  	g2im.kaspod\@gmail.com if you have troubles using KASpOD. KASpOD usage:')

pa.add_argument('--kmer',                     '-k',  dest='kmers_size',    default=25,       help='Probe length between 18 and 31. [25]')
pa.add_argument('--max_repet',               '-mr',  dest='max_repet',     default=8,        help='Maximum of allowed nucleotides repeats in probes. [8]')
pa.add_argument('--rm_N',                    '-N',   dest='rm_N',          default=False,    help='Remove sequences with N in input target fasta file [False]')
pa.add_argument('--nb_set',                  '-ns',  dest='nb_set',        default=1,        help='Number of generated probes set. [1]')
pa.add_argument('--edit',                     '-e',  dest='edit_distance', default=2,        help='Maximum number of differences (gaps and/or mismatches) allowed between a probe and its target (or non-target). [2]')
pa.add_argument('--maxcross',                 '-mc', dest='maxcross',      default=2,        help='Upper limit of tolerated cross-hybridizations to report a probe. [2]')       
pa.add_argument('--threads',                  '-t',  dest='nproc',         default=8,        help='Number of CPU available for calculation. [8]')
pa.add_argument('--match_against_nontarget', '-mau', dest='nmatch',        default=0,        help='At the begining, choosen number of allowed target probes wich match against non-target sequences. (creation of specific kmers) [0]')
pa.add_argument('--maxscore_degen',           '-md', dest='score',         default=64,       help='Maximum score for consensus degenerescence. An higher score can increase the probes number at the cost of efficiency. Max = 5000  Min = 0. [64]') 
pa.add_argument('--target',                          dest='target_file',                     help='target file in fasta')
pa.add_argument('--nontarget',                       dest='nontarget_file',                  help='non target file in fasta')          

#if no arguments given, show the help message in error output                  
if len(sys.argv)==1:
    pa.print_help(sys.stderr)
    sys.exit(1)             

#object to retrieve arguments with 'dest' (args.dest)           
args        = pa.parse_args() 
k_size      = int(args.kmers_size)
edit        = int(args.edit_distance)
maxcross    = int(args.maxcross)
target      = str(args.target_file)
nontarget   = str(args.nontarget_file)
degen_score = int(args.score)
CPU         = int(args.nproc)
halfCPU     = int(CPU/2)
mau         = int(args.nmatch)
max_repet   = str(args.max_repet)
choix       = int(args.nb_set)
remove_N    = bool(args.rm_N)
os.system("mkdir temp")

#On limite les tailles possibles des k-mers
if k_size > 60 or k_size < 18 :
	k_size = 25
if degen_score < 0 or degen_score > 5000 :
	degen_score = 64

if remove_N :
	#Suppression des N dans les séquences cible si demandé (modifie définitivement le fichier en input):
	os.system("mv "+target+" cible")
	with open (target, "w") as out :
		for record in SeqIO.parse("cible", "fasta"):
			if record.seq.count('N') == 1:
				print(record.format("fasta"), file=out)
	os.system("rm cible")			

#Dérépliquation des séquences dans les fichiers fasta de départ
with open(target+".format", 'w') as out :
	with open(target, 'r') as f1 : 
		for l in f1 : 
			l = l.strip()
			l = l.replace(' ','+')
			print(l, file=out)
with open(nontarget+".format", 'w') as out :			
	with open(nontarget, 'r') as f1 : 
		for l in f1 : 
			l = l.strip()
			l = l.replace(' ','+')	
			print(l, file=out)				
os.system("parallel 'vsearch --derep_fulllength {} --output {}.clean' ::: "+target+".format "+nontarget+".format")

os.system("mv "+target+".format.clean "+target)
os.system("mv "+nontarget+".format.clean "+nontarget)
os.system("rm "+target+".format* "+nontarget+".format*")
os.system("parallel 'seqkit sort -l {} > {}.clean' ::: "+target+" "+nontarget) #Tri des fasta dans l'ordre croissant de taille pour le calul de la position de la meilleure sonde
os.system("mv "+target+".clean "+target)
os.system("mv "+nontarget+".clean "+nontarget)

#Number of sequences in cleaned target file
nb_seq     = int(os.popen("grep -c '>' "+target).read())
with open ("nb","w") as out :
	print(nb_seq, file=out)

os.system("parallel './jellyfish count -t "+str(CPU)+" -o {}.res --mer-len "+str(k_size)+" --size 200M -C {}' ::: "+target+" "+nontarget) #2 out en res_0
os.system("./jellyfish dump "+target+".res -o "+target+".kmer.fas") #1 out en kmer.fas (entête = nb de seq target dans lequel le kmer apparait)

#Delete header from the file
with open (target,'r') as f1 :
	for l in f1 : 
		if l.startswith('>') :
			nomseqAsc.append(l.lstrip('>').strip())  #Récupération des séqquences par ordre croissant de taille

os.system("./jellyfish query -s "+target+".kmer.fas -o res.txt "+nontarget+".res")
	
#Je crois que l'on obtient le nombre de fois que l'on retrouve les query dans le groupe non target (ce qui permet de supprimer les k-mers qui ne sont pas spécifiques)	
#Dans ce cas, dans le for d'en dessous on ne garde que les séquences ciblées 0 fois. Pourquoi une autre boucle est faite ensuite pour supprimer les 1 alors qu'il ne reste que les 0 ?

#Production of the kmer specific file : 
with open (target+'.kmer_spe.fas','w') as output1:						#Ouverture du fichier en écriture 
	with open ('res.txt', 'r') as f1 :							  #Ouverture du fichier en lecture
		for l in f1 : 									    #Pour chaque ligne du fichier :
			l = l.strip()
			t = l.split(' ')                                                            #t devient une liste contenant la ligne parsée au niveau des espaces
			if int(t[1]) <= mau : 							    #Si les kmers matchent 0 ou mau fois contre les non cibles, on garde 
				c += 1
				print(">"+str(c)+"_"+t[1], t[0], sep = "\n", file = output1)        #Ecriture de la séquence dans le fichier de sortie

#CD-Hit (travailler sur plusieurs CPU rallonge la complexité temporelle sur petits jeux de données mais il faut faire des tests sur des jeux de données plus grands pour voir s'il est bénéfique ou pas de paralléliser le travail de cd-hit) :
#Attention, cette version python est plus couteuse en mémoire lors de l'utilisation de cd-hit (à cause de l'utilisation de plusieurs CPU)

os.system("cd-hit -c "+str(pclust)+" -d 0 -T "+str(CPU)+" -G 0 -aS 1 -aL 1 -g 1 -i "+target+".kmer_spe.fas -o "+target+".kmer_spe.clean."+str(pclust))

#Création des séquences consensus à partir des clusters et stockage dans un fichier : 

#Tout ce with open sert à récupérer une structure de type dictionnaire, id_seq, qui contient le numéro de sonde en clé et sa séquence en valeur
with open (target+".kmer_spe.fas",'r') as f1 : 
	for l in f1 : 					#Pour chaque ligne du fichier en input : 
		l = l.strip()
		if l.startswith(">") :				#Je met le numéro de sonde en clé
			nom = l.lstrip(">")
			id_seq[nom] = ""
		else :						#Quand je vois une séquence, je l'ajoute au dictionnaire pour la dernière clé définie
			id_seq[nom] = l

with open (target+".kmer_spe.clean."+str(pclust)+".clstr",'r') as f1 : 
	for l in f1 : 
		l = l.strip()
		if l.startswith('>Cluster'): 
			if cluster_id != "" : #If a first temp.fas was create               	
				L = list()
				with open ('tmp.fas','r') as f2 : 
					for l2 in f2:
						l2 = l2.strip()
						if not l2.startswith('>'):
							L.append(Seq(l2)) #each sequence in the cluster in a list (Bio.Seq)
				motif = motifs.create(L)
				cons  = motif.degenerate_consensus         #iupac consensus from the sequences of the list			
				scoretot = 1
				os.system("rm tmp.fas")
				for aa in list(cons) :
					scoretot = scoretot * degen[aa]
			if scoretot != 0 and scoretot <= degen_score : 
				h_header[cluster_id] = cons+";"+str(nb_seq)+";"

				if not re.search('[A|M|R|W|V|H|D|I|N]{'+max_repet+'}',str(cons)) or not re.search('[T|W|Y|K|H|D|B|I|N]{'+max_repet+'}',str(cons)) or not re.search('[C|M|S|Y|V|H|B|I|N]{'+max_repet+'}',str(cons)) or not re.search('[G|R|S|K|V|D|B|I|N]{'+max_repet+'}',str(cons)) :
					with open ("tmp_blast.fas","a") as out : #Fichier contenant les séquences consensus, en fasta, numérotée en header.
						print(">"+str(compt_id), cons, sep = '\n', file = out)
			cluster_id = l.split()[1]  #Number of the cluster
			clust      = False
			compt_id  += 1
		else: 
			clust = True
		if clust :	
			#temp.fas at the end of the if = id and sequence on the current Cluster
			regexp = re.search("\d\s+\d+aa, >(\d+_\d+)",l).group(1)   #Id corresponding to the header of the fasta file with kmers
			with open ('tmp.fas', 'a') as out :
				print(">"+regexp, id_seq[regexp], sep = "\n", file = out)


##for the last file of cluster unused in the loop (rend le fichier de résultat différent de celui de la version originale de kaspod car une erreur a été faite).
L = list()
with open ('tmp.fas','r') as f2 :               #On ouvre le dernier fichier généré par la boucle précédente, qui n'est pas traité
	for l2 in f2:
		l2 = l2.strip()
		if not l2.startswith('>'):
			L.append(Seq(l2))       #each sequence in the cluster in a list (Bio.Seq)
motif = motifs.create(L)
cons  = motif.degenerate_consensus              #iupac consensus from the sequences of the list			
scoretot = 1
os.system("rm tmp.fas")			
for aa in list(cons) :			        #Parcours de tous les nucléotides de la séquence consensus pour calculer le score de dégénérescence
	scoretot = scoretot * degen[aa]
if scoretot != 0 and scoretot <= degen_score :	#Si le score est inférieur au seuil limite, on remplit tmp_blast.fas avec la nouvelle séquence consensus 
	if not re.search('[A|M|R|W|V|H|D|I|N]{'+max_repet+'}',str(cons)) or not re.search('[T|W|Y|K|H|D|B|I|N]{'+max_repet+'}',str(cons)) or not re.search('[C|M|S|Y|V|H|B|I|N]{'+max_repet+'}',str(cons)) or not re.search('[G|R|S|K|V|D|B|I|N]{'+max_repet+'}',str(cons)) :
		with open ("tmp_blast.fas","a") as out : #Fichier contenant les séquences consensus, en fasta, numérotée en header.
			print(">"+str(compt_id), cons, sep = '\n', file = out)					

#On coupe les fichiers par tranches de 5000 lignes (2500 séquences) pour diviser le travail que l'on fournit à Patman : 
os.system("split tmp_blast.fas -d -l 5000 temp/tmp_blast_")

#PATMAN revision 1.2 (~1.7s sur petit jeu)
#+ calcul de sensibilité et spécificité 

files = listdir("temp")
for fic in files : #Pour chaque fichiers dans le répertoire temp
	os.system("./patman -e "+str(edit)+" -D "+target+" -P temp/"+fic+" -o res_sen.txt -a ; touch all_res.txt ; cat res_sen.txt >> all_res.txt") 
	with open ('res_sen.txt','r') as f1 : 	    #Lecture du fichier de résultat :
		for l in f1 : 			      #Pour chaque ligne du fichier : 	
			l = l.split('\t')             	#On enlève les sauts de ligne à la fin des lignes (uniquement pratique)
			ID_pos[l[1]] = l[2] 		#Position de chaque sonde sur la dernière séquence cible trouvée (essayer de faire en sorte que ca soit la + grande)
			match_sen[int(l[1])] += l[0]  	#On met dans un dictionnaire la séquence cible sur laquelle match la sonde sur cette ligne
			match_sen[int(l[1])] += "##"  	#Derrière le nom de la séquence cible que l'on vient d'ajouter à la sonde, on met ##
			deb[l[1]][l[0]]   = int(l[2])            	#Récupération de la position de début du match dans le fichier
	for k in sorted(match_sen.keys()) :           #Pour chaque valeur (ID de sondes) dans le dictionnaire remplit juste avant :
		t = (match_sen[k].split('##'))[:-1]   	#t prend la valeur du dict pour la sonde courante (k) transformée en liste parsée par les ## (liste de nom de target)
		t = set(t)				#Suppression des noms de target en double (si plusieurs versions d'une sonde dégénérée ont match sur la même target)
		sensi[k] = len(t)/nb_seq*100		#On attribue une valeur de sensibilité à chaque sondes (k) 

for fic in files : 
	os.system("./patman -e "+str(edit)+" -D "+nontarget+" -P temp/"+fic+" -o res_spe.txt -a") 
	with open ('res_spe.txt','r') as f1 : 
		for l in f1 : 
			l = l.split('\t')
			match_spe[int(l[1])] += l[0]	#Contient qui a cross hybridé avec la sonde en question
			match_spe[int(l[1])] += "##"
			seq_with_cross.add(l[1])        #Toutes les sondes qui ont des cross hybrid (outil de validation du script) 
	for k in sorted(match_spe.keys()) :
		t = (match_spe[k].split('##'))[:-1]
		t = set(t)
		speci[k] = len(t)			#On attribue une valeur de spécificité à chaque sondes (k) = au nombre de séquences target que la sonde cible

#A ce niveau, sensi et speci sont des structures remplies pour tous les fichiers présents dans temp et on peut se servir de ces dictionnaire sur le fichier tmp_blast.fas

#####Récupérer les séquences sans hybridation croisée#####
with open ('tmp_blast.fas','r') as f1 : 
	for l in f1 : 
		if l.startswith('>') :
			seq_all_probes.add(l.lstrip('>').rstrip())
			seqz          = l.lstrip('>').strip()
			nom_seq[seqz] = ""			
		else :
			nom_seq[seqz] = l.strip()
				
nb = 0
seq_without_cross = seq_all_probes - seq_with_cross
for v in speci :
	if speci[v] <= maxcross and speci[v] != 0 :
		nb+=1
		seq_without_cross.add(v) #On ajoute les séquences avec une cross-hybridization inférieure ou égale au seuil. 

#Il faut un fichier que me permet de récupérer chaque numéro de sonde, sa séquence, sa couverture, sa spécificité (CH) et les séquences cibles sur lesquelles elle match
with open ("temporary_results","w") as out :			
	print("#probes_ID",                     "probes_seq",       "coverage",    "probes_specificity", "sensi_match", sep = "\t", file = out)			
	for v in nom_seq : 
		s = 1
		for nt in nom_seq[v] :
			s = s * degen[nt]
		nom_degen[v] = s	
		if speci[int(v)] <= maxcross : 
			print (v,            nom_seq[str(v)], round(sensi[int(v)],4), speci[int(v)], match_sen[int(v)], sep = "\t", file = out)
			All_infos[str(v)] = [nom_seq[str(v)], round(sensi[int(v)],4), speci[int(v)], match_sen[int(v)]]
			
os.system("cat temporary_results | sort -k3 -k4 -nr > temporary_results2")			
Best_probe = (os.popen("cat temporary_results | sort -k3 -k4 -nr | head -1 | cut -f1").read()).strip() #Tri du fichier par ordre décroissant de couverture et de crossh
matchs_de_best_prob = set((All_infos[str(Best_probe)][3]).split("##"))

all_names = set()	#dictionnaire où je stocke tous les orgas conservés
for v in All_infos : 
	for org in All_infos[v][3].split('##') :
		all_names.add(org)
	
matchs_de_best_prob.discard('')
all_names.discard('')

for sondeID in All_infos :
	temposet = set()
	temposet = matchs_de_best_prob | set((All_infos[sondeID][3]).split('##'))  #Union des deux sets en un seul
	temposet.discard('')
	if temposet == all_names :
		Not_Found = False
		liste_min_probes.add(sondeID) #Set de toutes les sondes qui permettent de couvrir tout le jeu de données en addition à best_probe		

Jeu_de_sondes_final.add(Best_probe) #Structure contenant les sondes que l'on garde au final (ensemble de sondes minimal permettant de parcourir l'entièreté du jeu de données

if Not_Found == False :
	with open ('temp_file', 'w') as out : 
		for sonde in liste_min_probes :
			print(sonde, All_infos[sonde], file=out)
	Best_second_probe = os.popen("cat temp_file | sed -e 's:\[::g' | sed -e 's:\]::g' | sed -e 's:\,::g' | sed -e 's: :\t:g' | sort -k3,4 -n -r | head -1 | cut -f1").read()
	os.system("rm temp_file")
	if matchs_de_best_prob != all_names :
		Jeu_de_sondes_final.add(int(Best_second_probe)) #On récupère le contenu de cette variable dans un fichier plus tard
	
if Not_Found :
	while Not_Found == True : 
		val_init    = 0
		seuil_sensi = 0
		seuil_score = 90000		
		seuil_preci = 90000
		for sonde in All_infos : #Je cherche la sonde avec le plus de séquences couvertes qui ne le sont pas par Best_probe
			temposet = set((All_infos[sonde][3]).split('##')) - matchs_de_best_prob #Toute séquence couverte par sonde mais pas par best_probe et celles ajoutées*
			temposet.discard('')
			if len(temposet) > val_init :                            #Si la sonde courante couvre plus de séquences que ne couvre pas best_probe, comparé à la sonde précédente, on la garde
				val_init        = len(temposet)
				candidate_probe = set()
				candidate_probe.add(sonde)
			elif len(temposet) == val_init :
				candidate_probe.add(sonde)	
		for probe in candidate_probe :                                   #Je sélectionne ici la meilleure sonde en terme de couverture et de cross hybridation
			if float(All_infos[probe][1]) > float(seuil_sensi) :
				seuil_sensi = float(All_infos[probe][1])
				if int(All_infos[probe][2]) < int(seuil_preci) :
					seuil_preci = int(All_infos[probe][2])
					score_degen_tot = 1
					for nt in nom_seq[probe] :
						score_degen_tot = score_degen_tot * degen[nt]
					if score_degen_tot < seuil_score :
						seuil_score = score_degen_tot
						save_probe  = str(probe)         #A la fin du parcours de la liste, il reste la sonde avec les meilleures caractéristiques dans cette variable
		Jeu_de_sondes_final.add(save_probe)	                         #Structure vide à ce moment car soit cette boucle soit la précédente tourne mais pas les deux sur le même jeu.		
		temposet2 = set(All_infos[save_probe][3].split('##'))
		temposet2.discard('')
		matchs_de_best_prob = matchs_de_best_prob | temposet2	         #j'ajoute les séquences qu'elle cible à match_best_probe*
		if len(matchs_de_best_prob) == len(all_names) :                  #je vérifie que matchs_de_best_prob est différent de all_name, sinon j'arrète la boucle et je récupère la liste de sonde
			Not_Found = False
nb_setdesondesfinal = defaultdict(set)

#On peut rétablir l'écriture des sets primaires dans un fichier si besoin en ajoutantici le with open et en décommentant les print en ajoutant la bonne sortie

#print('ID','Seq','coverage', 'cross_hyb', 'start_on_target', 'probe_length', 'degen_score', 'targeted_target_seq', sep='\t')
for probe in Jeu_de_sondes_final :
	Each_best_probes[str(probe)] = [All_infos[str(probe)][0], 
				   All_infos[str(probe)][1], 
				   All_infos[str(probe)][2], 
				   ID_pos[str(probe)], 
				   len(nom_seq[str(probe)]), 
				   nom_degen[str(probe)], 
				   set(All_infos[str(probe)][3].replace('##',' ## ').rstrip(' ##').split(' ## '))]

if choix >= 2 :	      
	for i in range(2,choix+1) :
		nb = 0
		with open ('temporary_results2', 'r') as f1 : #Ajouter autant de jeu de séquences minimals que l'utilisateur en demande
			for l in f1 : 
				nb += 1
				if nb == i :
					main_probe = l.split('\t')[0] #meilleure sonde après celle qui la précede, on cherche ses sondes complémentaires
					matchs_de_main_prob = set((All_infos[str(main_probe)][3]).split("##"))
					matchs_de_main_prob.discard('')
					#Pour chaque sonde qui va passer dans main_probe
					Not_Found = True
					while Not_Found :
						val_init    = 0
						seuil_sensi = 0
						seuil_score = 90000		
						seuil_preci = 90000
						for sonde in All_infos : 
							temposet = set((All_infos[sonde][3]).split('##')) - matchs_de_main_prob 
							temposet.discard('')
							if len(temposet) > val_init :                            
								val_init        = len(temposet)
								candidate_probe = set()
								candidate_probe.add(sonde)
							elif len(temposet) == val_init :
								candidate_probe.add(sonde)	
						for probe in candidate_probe :                                  
							if float(All_infos[probe][1]) > float(seuil_sensi) :
								seuil_sensi = float(All_infos[probe][1])
								if int(All_infos[probe][2]) < int(seuil_preci) :
									seuil_preci = int(All_infos[probe][2])
									score_degen_tot = 1
									for nt in nom_seq[probe] :
										score_degen_tot = score_degen_tot * degen[nt]
									if score_degen_tot < seuil_score :
										seuil_score = score_degen_tot
										save_probe  = str(probe) 
						nb_setdesondesfinal[i].add(save_probe)
						linknames[i] = main_probe
						temposet2 = set(All_infos[save_probe][3].split('##'))
						temposet2.discard('')
						matchs_de_main_prob = matchs_de_main_prob | temposet2	        
						if len(matchs_de_main_prob) == len(all_names) :                 
							Not_Found = False
for sonde in nb_setdesondesfinal :
	Each_best_probes[str(linknames[sonde])] = [nom_seq[str(linknames[sonde])],
						round(sensi[int(linknames[sonde])],4), 
	      					round(speci[int(linknames[sonde])],4), 
	      					ID_pos[linknames[sonde]],
	      					len(nom_seq[str(linknames[sonde])]),
	      					nom_degen[linknames[sonde]],
	      					set(All_infos[linknames[sonde]][3].replace('##',' ## ').rstrip(' ##').split(' ## '))]
	      					
	nb_setdesondesfinal[sonde] = list(nb_setdesondesfinal[sonde])
	for one in range(len(nb_setdesondesfinal[sonde])) :
		Each_best_probes[str(nb_setdesondesfinal[sonde][one])] = [nom_seq[nb_setdesondesfinal[sonde][one]], 
					   round(sensi[int(nb_setdesondesfinal[sonde][one])],4), 
					   round(speci[int(nb_setdesondesfinal[sonde][one])],4),
					   ID_pos[nb_setdesondesfinal[sonde][one]], 
					   len(nom_seq[nb_setdesondesfinal[sonde][one]]), 
					   nom_degen[nb_setdesondesfinal[sonde][one]],
					   set(All_infos[nb_setdesondesfinal[sonde][one]][3].replace('##',' ## ').rstrip(' ##').split(' ## '))]		
	
#A la fin, each best probes contient toutes les sondes des sets minimaux que l'on a définit et les informations qui les caractérisent
#On veut utiliser le nom des sondes sélectionnées pour récupérer toutes les lignes du fichier patman qui les cooncernent et supprimer les autres

with open ("sondes.txt", "a") as out : 
	for ids in Each_best_probes : 
		print(ids, file=out)
print("Fin de la production des sets minimaux, début de la sélection")

#tempo est le nouveau fichier patman avec seulement les sondes qui nous interessent et l'info de séquences et ID.
os.system("while read line ; do awk -v line=$line -F'\t' 'BEGIN{OFS=\"\t\"} $2==line{print $1,$2,$3}' all_res.txt | sort | uniq | gzip >> tempo; done < sondes.txt")

#On applique ensuite l'algorithme bash de sélection des sondes sur ce fichier patman au format gzip
#(J'utilise directement les scripts séparément)


os.system("rm all_res.txt sondes_* tempo_*")
os.system("rm res.txt res_sen.txt res_spe.txt temporary_results temporary_results2 "+nontarget+".res "+target+".*")

