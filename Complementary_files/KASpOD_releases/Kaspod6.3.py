#!/usr/bin/python

#KASpOD -- A k-mer based algorithm for high-specific oligonucleotide design.
#Copyright (C) 2012 Nicolas Parisot
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#Contact: g2im.kaspod@gmail.com

#Version de kaspod dans laquelle j'essaie de paralléliser la production des sets minimaux

from joblib import Parallel, delayed			
from Bio.Seq import Seq					#To work with fasta files with efficiency
from Bio import motifs					#Clusters alignment and consensus sequences
from Bio import AlignIO					#Clusters alignment and consensus sequences
from Bio.Align import AlignInfo				#Clusters alignment and consensus sequences
from Bio import SeqIO
from Bio import SearchIO
from collections import defaultdict			#Dictionaries with default values to ease computation
from os import listdir					#To interact with files and directories on the system
import argparse  					#Command line argument parser
import sys       					#Allow interaction with linux system
import os	 					#Allow bash command in a python script
import re						#For regular expression

pclust              = 0.88    		#Similarity threshold for clusters
c                   = 0			
scoretot            = 0
compt_id            = 0
clust               = False	
Not_Found           = True
cluster_id          = ""
nomseqAsc           = list()
all_names           = set()
liste_min_probes    = set()
seq_with_cross      = set()
seq_all_probes      = set()
candidate_probe     = set()
Jeu_de_sondes_final = set()
h_header            = dict()
id_seq              = dict()
deb                 = defaultdict(lambda: defaultdict(int))
ID_pos              = dict()
nom_degen           = dict()
linknames           = dict()
nom_seq             = dict()
sensi               = defaultdict(int)
speci               = defaultdict(int)
match_spe           = defaultdict(str)
match_sen           = defaultdict(str)
All_infos           = defaultdict(list)	

#Score de dégenerescence établit à partir de la table IUB (Code IUPAC)
degen = dict(A=1,C=1,G=1,T=1,M=2,R=2,W=2,S=2,Y=2,K=2,V=3,H=3,D=3,B=3,I=4,N=4)

#Setup des arguments de la ligne de commande             
pa = argparse.ArgumentParser(description='KASpOD -- A k-mer based algorithm for high-specific oligonucleotide design. Version 0.3 (09/2012). Feel free to contact  	g2im.kaspod\@gmail.com if you have troubles using KASpOD. KASpOD usage:')

pa.add_argument('--kmer',                     '-k',  dest='kmers_size',    default=25,       help='Probe length between 18 and 31. [25]')
pa.add_argument('--max_repet',               '-mr',  dest='max_repet',     default=8,        help='Maximum of allowed nucleotides repeats in probes. [8]')
pa.add_argument('--rm_N',                    '-N',   dest='rm_N',          default=False,    help='Remove sequences with N in input target fasta file [False]')
pa.add_argument('--nb_set',                  '-ns',  dest='nb_set',        default=1,        help='Number of generated probes set. [1]')
pa.add_argument('--edit',                     '-e',  dest='edit_distance', default=2,        help='Maximum number of differences (gaps and/or mismatches) allowed between a probe and its target (or non-target). [2]')
pa.add_argument('--maxcross',                 '-mc', dest='maxcross',      default=2,        help='Upper limit of tolerated cross-hybridizations to report a probe. [2]')       
pa.add_argument('--threads',                  '-t',  dest='nproc',         default=8,        help='Number of CPU available for calculation. [8]')
pa.add_argument('--match_against_nontarget', '-mau', dest='nmatch',        default=0,        help='At the begining, choosen number of allowed target probes wich match against non-target sequences. (creation of specific kmers) [0]')
pa.add_argument('--maxscore_degen',           '-md', dest='score',         default=64,       help='Maximum score for consensus degenerescence. An higher score can increase the probes number at the cost of efficiency. Max = 5000  Min = 0. [64]') 
pa.add_argument('--target',                          dest='target_file',                     help='target file in fasta')
pa.add_argument('--nontarget',                       dest='nontarget_file',                  help='non target file in fasta')          

#Si le script est lancé sans argument, retourne un message d'erreur               
if len(sys.argv)==1:
    pa.print_help(sys.stderr)
    sys.exit(1)             

#Conversion des arguments donnés en variables python          
args        = pa.parse_args() 
k_size      = int(args.kmers_size)
edit        = int(args.edit_distance)
maxcross    = int(args.maxcross)
target      = str(args.target_file)
nontarget   = str(args.nontarget_file)
degen_score = int(args.score)
CPU         = int(args.nproc)
halfCPU     = int(CPU/2)
mau         = int(args.nmatch)
max_repet   = str(args.max_repet)
choix       = int(args.nb_set)
remove_N    = bool(args.rm_N)
os.system("mkdir temp")

#On limite les tailles possibles des k-mers pour s'adapter aux limites des programmes utilisés
if k_size > 60 or k_size < 18 :
	k_size = 25
if degen_score < 0 or degen_score > 5000 :
	degen_score = 64

if remove_N :
	#Suppression des N dans les séquences cible si demandé (modifie définitivement le fichier en input):
	os.system("mv "+target+" cible")
	with open (target, "w") as out :
		for record in SeqIO.parse("cible", "fasta"):
			if record.seq.count('N') == 1:
				print(record.format("fasta"), file=out)
	os.system("rm cible")			

#Dérépliquation des séquences dans les fichiers fasta de départ
with open(target+".format", 'w') as out :
	with open(target, 'r') as f1 : 
		for l in f1 : 
			l = l.strip()
			l = l.replace(' ','+')
			print(l, file=out)
with open(nontarget+".format", 'w') as out :			
	with open(nontarget, 'r') as f1 : 
		for l in f1 : 
			l = l.strip()
			l = l.replace(' ','+')	
			print(l, file=out)				
os.system("parallel 'vsearch --derep_fulllength {} --output {}.clean' ::: "+target+".format "+nontarget+".format")

os.system("mv "+target+".format.clean "+target)
os.system("mv "+nontarget+".format.clean "+nontarget)
os.system("rm "+target+".format* "+nontarget+".format*")
os.system("parallel 'seqkit sort -l {} > {}.clean' ::: "+target+" "+nontarget) #Tri des fasta dans l'ordre croissant de taille pour le calul de la position de la meilleure sonde
os.system("mv "+target+".clean "+target)
os.system("mv "+nontarget+".clean "+nontarget)

#Calcul du nombre de séquences dans le fichier cible nettoyé
nb_seq     = int(os.popen("grep -c '>' "+target).read())
with open ("nb","w") as out :
	print(nb_seq, file=out)

#Production des k-mers des deux groupes
os.system("parallel './jellyfish count -t "+str(CPU)+" -o {}.res --mer-len "+str(k_size)+" --size 200M -C {}' ::: "+target+" "+nontarget)
os.system("./jellyfish dump "+target+".res -o "+target+".kmer.fas") 

#Stockage des noms des séquences par ordre croissant de taille (Pour ordonner des boucles, trouver la longueur de la séquence la plus longues, ...) 
with open (target,'r') as f1 :
	for l in f1 : 
		if l.startswith('>') :
			nomseqAsc.append(l.lstrip('>').strip())

#Comptage du nombre de match des k-mers sur les séquences non cibles
os.system("./jellyfish query -s "+target+".kmer.fas -o res.txt "+nontarget+".res")


#Production of the kmer specific file (On ne garde que les k-mers qui ont moins de "mau" match contre les non cibles) : 
with open (target+'.kmer_spe.fas','w') as output1:						#Ouverture du fichier en écriture 
	with open ('res.txt', 'r') as f1 :							                            #Ouverture du fichier en lecture
		for l in f1 : 									                                                #Pour chaque ligne du fichier :
			l = l.strip()
			t = l.split(' ')                                                                            #t devient une liste contenant la ligne parsée au niveau des espaces
			if int(t[1]) <= mau : 						                                	    #Si les kmers matchent 0 ou mau fois contre les non cibles, on garde 
				c += 1
				print(">"+str(c)+"_"+t[1], t[0], sep = "\n", file = output1)        #Ecriture de la séquence dans le fichier de sortie (format fasta)

os.system("cd-hit -c "+str(pclust)+" -d 0 -T "+str(CPU)+" -G 0 -aS 1 -aL 1 -g 1 -M 20000 -i "+target+".kmer_spe.fas -o "+target+".kmer_spe.clean."+str(pclust))

#Création des séquences consensus à partir des clusters et stockage dans un fichier : 

#Tout ce with open sert à récupérer une structure de type dictionnaire, id_seq, qui contient le numéro de sonde en clé et sa séquence en valeur (pour tous les k-mers spécifiques)
with open (target+".kmer_spe.fas",'r') as f1 : 
	for l in f1 : 					#Pour chaque ligne du fichier en input : 
		l = l.strip()
		if l.startswith(">") :				#Je met le numéro de sonde en clé
			nom = l.lstrip(">")
			id_seq[nom] = ""
		else :						#Quand je vois une séquence, je l'ajoute au dictionnaire pour la dernière clé définie
			id_seq[nom] = l

#Ouverture du fichier de clusters et création d'une séquence consensus pour chaque cluster. Les séquences obtenues sont stockées dans tmp_blast.fas
with open (target+".kmer_spe.clean."+str(pclust)+".clstr",'r') as f1 : 
	for l in f1 : 
		l = l.strip()
		if l.startswith('>Cluster'): 
			if cluster_id != "" : #If a first temp.fas was create               	
				L = list()
				with open ('tmp.fas','r') as f2 : 
					for l2 in f2:
						l2 = l2.strip()
						if not l2.startswith('>'):
							L.append(Seq(l2)) #each sequence in the cluster in a list (Bio.Seq)
				motif = motifs.create(L)
				cons  = motif.degenerate_consensus         #iupac consensus from the sequences of the list			
				scoretot = 1
				os.system("rm tmp.fas")
				for aa in list(cons) :
					scoretot = scoretot * degen[aa]
			if scoretot != 0 and scoretot <= degen_score : 
				h_header[cluster_id] = cons+";"+str(nb_seq)+";"

				if not re.search('[A|M|R|W|V|H|D|I|N]{'+max_repet+'}',str(cons)) or not re.search('[T|W|Y|K|H|D|B|I|N]{'+max_repet+'}',str(cons)) or not re.search('[C|M|S|Y|V|H|B|I|N]{'+max_repet+'}',str(cons)) or not re.search('[G|R|S|K|V|D|B|I|N]{'+max_repet+'}',str(cons)) :
					with open ("tmp_blast.fas","a") as out : #Fichier contenant les séquences consensus, en fasta, numérotée en header.
						print(">"+str(compt_id), cons, sep = '\n', file = out)
			cluster_id = l.split()[1]  #Number of the cluster
			clust      = False
			compt_id  += 1
		else: 
			clust = True
		if clust :	
			#temp.fas at the end of the if = id and sequence on the current Cluster
			regexp = re.search("\d\s+\d+aa, >(\d+_\d+)",l).group(1)   #Id corresponding to the header of the fasta file with kmers
			with open ('tmp.fas', 'a') as out :
				print(">"+regexp, id_seq[regexp], sep = "\n", file = out)


#Le dernier cluster n'est pas prit en charge par la boucle précédente, il faut réutiliser le même code une dernière fois pour traiter le dernier fichier de cluster
L = list()
with open ('tmp.fas','r') as f2 :               #On ouvre le dernier fichier généré par la boucle précédente, qui n'est pas traité
	for l2 in f2:
		l2 = l2.strip()
		if not l2.startswith('>'):
			L.append(Seq(l2))                          #each sequence in the cluster in a list (Bio.Seq)
motif = motifs.create(L)
cons  = motif.degenerate_consensus       #iupac consensus from the sequences of the list			
scoretot = 1
os.system("rm tmp.fas")			
for aa in list(cons) :			                     #Parcours de tous les nucléotides de la séquence consensus pour calculer le score de dégénérescence
	scoretot = scoretot * degen[aa]
if scoretot != 0 and scoretot <= degen_score :	#Si le score est inférieur au seuil limite, on remplit tmp_blast.fas avec la nouvelle séquence consensus 
	if not re.search('[A|M|R|W|V|H|D|I|N]{'+max_repet+'}',str(cons)) or not re.search('[T|W|Y|K|H|D|B|I|N]{'+max_repet+'}',str(cons)) or not re.search('[C|M|S|Y|V|H|B|I|N]{'+max_repet+'}',str(cons)) or not re.search('[G|R|S|K|V|D|B|I|N]{'+max_repet+'}',str(cons)) :
		with open ("tmp_blast.fas","a") as out : #Fichier contenant les séquences consensus, en fasta, numérotée en header.
			print(">"+str(compt_id), cons, sep = '\n', file = out)					

#On coupe les fichiers par tranches de 5000 lignes (2500 séquences) pour diviser le travail que l'on fournit à Patman : 
os.system("split tmp_blast.fas -d -l 5000 temp/tmp_blast_")

#PATMAN revision 1.2
#+ calcul de sensibilité et spécificité 


a = defaultdict(int)

files = listdir("temp")

def process_patman_sen(f):
	global sensi
	global match_sen
	os.system("./patman -e "+str(edit)+" -D "+target+" -P temp/"+f+" -o res_sen"+f+".txt -a ; touch all_res.txt ; cat res_sen"+f+".txt >> all_res.txt") 	
	with open ("res_sen"+f+".txt","r") as f1 :
		for l in f1 : 	
			l = l.split('\t')		
			match_sen[int(l[1])] += l[0]       	
			match_sen[int(l[1])] += "##"  	   
	for k in sorted(match_sen.keys()) :      
		t = (match_sen[k].split('##'))[:-1]
		t = set(t) 
		sensi[k] = len(t)/nb_seq*100		

def process_patman_spe(f):
	global speci
	global match_spe
	global seq_with_cross
	os.system("./patman -e "+str(edit)+" -D "+nontarget+" -P temp/"+f+" -o res_spe"+f+".txt -a") 
	with open ("res_spe"+f+".txt","r") as f1 : 
		for l in f1 : 
			l = l.split('\t')
			match_spe[int(l[1])] += l[0]	        #Contient le nom des cibles qui ont "cross hybridé" avec la sonde courante
			match_spe[int(l[1])] += "##"
			seq_with_cross.add(l[1])                 #Toutes les sondes qui ont des cross hybrid (outil de validation du script) 
	for k in sorted(match_spe.keys()) :
		t = (match_spe[k].split('##'))[:-1]
		t = set(t)
		speci[k] = len(t)	

Parallel(n_jobs=CPU, backend='threading')(delayed(process_patman_sen)(f) for f in files) #threading ralentit la parallélisation mais permet de conserver les variables
Parallel(n_jobs=CPU, backend='threading')(delayed(process_patman_spe)(f) for f in files)

print("Fin Patman. Extraction des sets minimaux en cours")
#Traitement du fichier de résultat patman et création des sets minimaux de sondes

#Récupérer les séquences sans hybridation croisée
with open ('tmp_blast.fas','r') as f1 : 
	for l in f1 : 
		if l.startswith('>') :
			seq_all_probes.add(l.lstrip('>').rstrip())
			seqz          = l.lstrip('>').strip()
			nom_seq[seqz] = ""			
		else :
			nom_seq[seqz] = l.strip()

nb = 0
seq_without_cross = seq_all_probes - seq_with_cross
for v in speci :
	if speci[v] <= maxcross and speci[v] != 0 :
		nb+=1
		seq_without_cross.add(v) #On ajoute les séquences avec une cross-hybridation inférieure ou égale au seuil. 

#Il faut un fichier que me permet de récupérer chaque numéro de sonde, sa séquence, sa couverture, sa spécificité (CH) et les séquences cibles sur lesquelles elle match
with open ("temporary_results","w") as out :			
	print("#probes_ID",                     "probes_seq",       "coverage",    "probes_specificity", "sensi_match", sep = "\t", file = out)			
	for v in nom_seq : 
		s = 1
		for nt in nom_seq[v] :
			s = s * degen[nt]
		nom_degen[v] = s	
		if speci[int(v)] <= maxcross : 
			print (v,            nom_seq[str(v)], round(sensi[int(v)],4), speci[int(v)], match_sen[int(v)], sep = "\t", file = out)
			All_infos[str(v)] = [nom_seq[str(v)], round(sensi[int(v)],4), speci[int(v)], match_sen[int(v)]]

os.system("cat temporary_results | sort -k3 -k4 -nr > temporary_results2")			
#matchs_de_best_prob = set((All_infos[str(Best_probe)][3]).split("##"))

#Dictionnaire où je stocke tous les noms de toutes les cibles
all_names = set()
for v in All_infos : 
	for org in All_infos[v][3].split('##') :
		all_names.add(org)

#matchs_de_best_prob.discard('')
all_names.discard('')

n = 0
reference_p = list()
with open ('temporary_results2','r') as f1 : 
	for l in f1 :
		l = l.strip()
		n += 1
		if n <= choix : 
			reference_p.append(l.split('\t')[0])
#On a la liste des sondes de référence de nos sets. On veut les fournir en argument à une fonction qui va permettre de créer un set minimal à partir de son argument
#La fonction doit nous permettre de récupérer rapidement toutes les sondes du set
#La difficulté est de récupérer ensuite toutes les sondes de tous les sets dans la même structure de données python sans passer par de l'écriture de fichiers. 
#r = reference = argument de la fonction

def find_min_set(r) :
	r = r #Sonde de référence
	matchs_de_main_prob = set((All_infos[str(r)][3]).split("##"))
	matchs_de_main_prob.discard('')
	nb_setdesondesfinal = set()
	Each_best_probes = list()
	candidate_probe = set()
	#Pour chaque sonde qui va passer dans main_probe
	Not_Found = True
	while Not_Found :
		val_init    = 0
		seuil_sensi = 90000 #Si on inverse le if dans la sélection des sondes candidates, on met 0 ou 90000 ici
		seuil_score = 90000		
		seuil_preci = 90000
		for sonde in All_infos : 
			temposet = set((All_infos[sonde][3]).split('##')) - matchs_de_main_prob 
			temposet.discard('')
			if len(temposet) > val_init :                            
				val_init        = len(temposet)
				candidate_probe = set()
				candidate_probe.add(sonde)
			elif len(temposet) == val_init :
				candidate_probe.add(sonde)	
		if len(candidate_probe) > 1 :		
			for probe in candidate_probe : #Quand 2 sondes ou plus couvrent le même nombre de cibles que le jeu déja selectionné ne couvre pas, on choisit la sonde qui a le moins d'hybridation croisée et de dégenerescence. Les sondes candidates ont la même couverture de cibles que la ref ne couvre pas mais une couverture globale différente. Il peut être intéressant de choisir la sonde candidate qui a la moins bonne couverture globale pour limiter la redondance (< dans le if) ou celle qui a la meilleure (> dans le if) /!\ Ne pas oublier l'initialisation de seuil sensi /!\                            
#				if float(All_infos[probe][1]) > float(seuil_sensi) :                              
				if float(All_infos[probe][1]) < float(seuil_sensi) :
					seuil_sensi = float(All_infos[probe][1])
					if int(All_infos[probe][2]) < int(seuil_preci) :
						seuil_preci = int(All_infos[probe][2])
						score_degen_tot = 1
						for nt in nom_seq[probe] :
							score_degen_tot = score_degen_tot * degen[nt]
						if score_degen_tot < seuil_score :
							seuil_score = score_degen_tot
							save_probe  = str(probe) 
		else:
			save_probe = list(candidate_probe)[0]	
		nb_setdesondesfinal.add(save_probe) #J'ai trouvé la meilleure sonde complémentaire et je stocke dans cette variable
		temposet2 = set(All_infos[save_probe][3].split('##'))
		temposet2.discard('')
		matchs_de_main_prob = matchs_de_main_prob | temposet2	 #j'adapte les cibles couvertes pour le prochain tour de while       
		if len(matchs_de_main_prob) == len(all_names) :                 
			Not_Found = False
	for sonde in nb_setdesondesfinal : #Je sauvegarde chaque sonde que j'ai trouvé pour la référence et la référence dans une liste = output de la fonction
		Each_best_probes.append(str(sonde))	
	with open ("sondes_temp.txt","a")as out:
		for v in set(Each_best_probes) :
			print(v, file=out)	
	

#Le parallel semble fonctionner, il faut tester sur les données midori si les résultats sont pertinentns et justes e que les sondes des sets minimaux sont toujours pertinement sélectionnées malgré les changements dans le script. Le format du fichier sonde est inchangé donc ce script fonctionne avec tous les autres qui suivent dans la chaine de design de sondes. 

results = Parallel(n_jobs=CPU)(delayed(find_min_set)(ref) for ref in reference_p)
os.system("cat sondes_temp.txt | sort -n | uniq > sondes.txt")

print(f'Nombre de sets produits : {len(results)}')

os.system("rm res_sen.txt res_spe.txt temporary_results temporary_results2 "+nontarget+".res "+target+".*")
