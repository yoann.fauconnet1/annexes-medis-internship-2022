#!/usr/bin/python

#KASpOD -- A k-mer based algorithm for high-specific oligonucleotide design.
#Copyright (C) 2012 Nicolas Parisot
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#Contact: g2im.kaspod@gmail.com

from Bio.Seq import Seq					#To work with fasta files with efficiency
from Bio import motifs					#Clusters alignment and consensus sequences
from Bio import AlignIO					#Clusters alignment and consensus sequences
from Bio.Align import AlignInfo				#Clusters alignment and consensus sequences
from Bio import SeqIO
from Bio import SearchIO
from collections import defaultdict			#Dictionaries with default values to ease computation
from os import listdir					#To interact with files and directories on the system
import argparse  					#Command line argument parser
import sys       					#Allow interaction with linux system
import os	 					#Allow bash command in a python script
import re						#For regular expression

pclust              = 0.88    		#Similarity threshold for clusters
c                   = 0			
scoretot            = 0
compt_id            = 0
clust               = False	
Not_Found           = True
cluster_id          = ""
nomseqAsc           = list()
liste_cible         = list()
all_names           = set()
liste_min_probes    = set()
seq_with_cross      = set()
seq_all_probes      = set()
candidate_probe     = set()
Jeu_de_sondes_final = set()
h_header            = dict()
id_seq              = dict()
deb                 = defaultdict(lambda: defaultdict(int))
ID_pos              = dict()
nom_degen           = dict()
linknames           = dict()
nom_seq             = dict()
Each_best_probes    = defaultdict(list)
sensi               = defaultdict(int)
speci               = defaultdict(int)
match_spe           = defaultdict(str)
match_sen           = defaultdict(list)
cible_sonde         = defaultdict(list)
All_infos           = defaultdict(list)	

#Score de dégenerescence établit à partir de la table IUB (Code IUPAC)
degen = dict(A=1,C=1,G=1,T=1,M=2,R=2,W=2,S=2,Y=2,K=2,V=3,H=3,D=3,B=3,I=4,N=4)

#Command line argument parsing to create script options             
pa = argparse.ArgumentParser(description='KASpOD -- A k-mer based algorithm for high-specific oligonucleotide design. Version 0.3 (09/2012). Feel free to contact  	g2im.kaspod\@gmail.com if you have troubles using KASpOD. KASpOD usage:')

pa.add_argument('--kmer',                     '-k',  dest='kmers_size',    default=25,       help='Probe length between 18 and 31. [25]')
pa.add_argument('--max_repet',               '-mr',  dest='max_repet',     default=8,        help='Maximum of allowed nucleotides repeats in probes. [8]')
pa.add_argument('--rm_N',                    '-N',   dest='rm_N',          default=False,    help='Remove sequences with N in input target fasta file [False]')
pa.add_argument('--nb_set',                  '-ns',  dest='nb_set',        default=1,        help='Number of generated probes set. [1]')
pa.add_argument('--edit',                     '-e',  dest='edit_distance', default=2,        help='Maximum number of differences (gaps and/or mismatches) allowed between a probe and its target (or non-target). [2]')
pa.add_argument('--maxcross',                 '-mc', dest='maxcross',      default=2,        help='Upper limit of tolerated cross-hybridizations to report a probe. [2]')       
pa.add_argument('--threads',                  '-t',  dest='nproc',         default=8,        help='Number of CPU available for calculation. [8]')
pa.add_argument('--match_against_nontarget', '-mau', dest='nmatch',        default=0,        help='At the begining, choosen number of allowed target probes wich match against non-target sequences. (creation of specific kmers) [0]')
pa.add_argument('--maxscore_degen',           '-md', dest='score',         default=64,       help='Maximum score for consensus degenerescence. An higher score can increase the probes number at the cost of efficiency. Max = 5000  Min = 0. [64]') 
pa.add_argument('--target',                          dest='target_file',                     help='target file in fasta')
pa.add_argument('--nontarget',                       dest='nontarget_file',                  help='non target file in fasta')          

#if no arguments given, show the help message in error output                  
if len(sys.argv)==1:
    pa.print_help(sys.stderr)
    sys.exit(1)             

#object to retrieve arguments with 'dest' (args.dest)           
args        = pa.parse_args() 
k_size      = int(args.kmers_size)
edit        = int(args.edit_distance)
maxcross    = int(args.maxcross)
target      = str(args.target_file)
nontarget   = str(args.nontarget_file)
degen_score = int(args.score)
CPU         = int(args.nproc)
halfCPU     = int(CPU/2)
mau         = int(args.nmatch)
max_repet   = str(args.max_repet)
choix       = int(args.nb_set)
remove_N    = bool(args.rm_N)
os.system("mkdir temp")

#On limite les tailles possibles des k-mers
if k_size > 60 or k_size < 18 :
	k_size = 25
if degen_score < 0 or degen_score > 5000 :
	degen_score = 64

if remove_N :
	#Suppression des N dans les séquences cible si demandé (modifie définitivement le fichier en input):
	os.system("mv "+target+" cible")
	with open (target, "w") as out :
		for record in SeqIO.parse("cible", "fasta"):
			if record.seq.count('N') == 1:
				print(record.format("fasta"), file=out)
	os.system("rm cible")			

#Dérépliquation des séquences dans les fichiers fasta de départ
with open(target+".format", 'w') as out :
	with open(target, 'r') as f1 : 
		for l in f1 : 
			l = l.strip()
			l = l.replace(' ','+')
			print(l, file=out)
with open(nontarget+".format", 'w') as out :			
	with open(nontarget, 'r') as f1 : 
		for l in f1 : 
			l = l.strip()
			l = l.replace(' ','+')	
			print(l, file=out)				
os.system("parallel 'vsearch --derep_fulllength {} --output {}.clean' ::: "+target+".format "+nontarget+".format")

os.system("mv "+target+".format.clean "+target)
os.system("mv "+nontarget+".format.clean "+nontarget)
os.system("rm "+target+".format* "+nontarget+".format*")
os.system("parallel 'seqkit sort -l {} > {}.clean' ::: "+target+" "+nontarget) #Tri des fasta dans l'ordre croissant de taille pour le calul de la position de la meilleure sonde
os.system("mv "+target+".clean "+target)
os.system("mv "+nontarget+".clean "+nontarget)

#Number of sequences in cleaned target file
nb_seq     = int(os.popen("grep -c '>' "+target).read())

os.system("parallel './jellyfish count -t "+str(CPU)+" -o {}.res --mer-len "+str(k_size)+" --size 200M -C {}' ::: "+target+" "+nontarget) #2 out en res_0
os.system("./jellyfish dump "+target+".res -o "+target+".kmer.fas") #1 out en kmer.fas (entête = nb de seq target dans lequel le kmer apparait)

#Delete header from the file
with open (target,'r') as f1 :
	for l in f1 : 
		if l.startswith('>') :
			nomseqAsc.append(l.lstrip('>').strip())  #Récupération des séqquences par ordre croissant de taille

os.system("./jellyfish query -s "+target+".kmer.fas -o res.txt "+nontarget+".res")
	
#Je crois que l'on obtient le nombre de fois que l'on retrouve les query dans le groupe non target (ce qui permet de supprimer les k-mers qui ne sont pas spécifiques)	
#Dans ce cas, dans le for d'en dessous on ne garde que les séquences ciblées 0 fois. Pourquoi une autre boucle est faite ensuite pour supprimer les 1 alors qu'il ne reste que les 0 ?

#Production of the kmer specific file : 
with open (target+'.kmer_spe.fas','w') as output1:						#Ouverture du fichier en écriture 
	with open ('res.txt', 'r') as f1 :							  #Ouverture du fichier en lecture
		for l in f1 : 									    #Pour chaque ligne du fichier :
			l = l.strip()
			t = l.split(' ')                                                            #t devient une liste contenant la ligne parsée au niveau des espaces
			if int(t[1]) <= mau : 							    #Si les kmers matchent 0 ou mau fois contre les non cibles, on garde 
				c += 1
				print(">"+str(c)+"_"+t[1], t[0], sep = "\n", file = output1)        #Ecriture de la séquence dans le fichier de sortie

#CD-Hit (travailler sur plusieurs CPU rallonge la complexité temporelle sur petits jeux de données mais il faut faire des tests sur des jeux de données plus grands pour voir s'il est bénéfique ou pas de paralléliser le travail de cd-hit) :
#Attention, cette version python est plus couteuse en mémoire lors de l'utilisation de cd-hit (à cause de l'utilisation de plusieurs CPU)

os.system("cd-hit -c "+str(pclust)+" -d 0 -T "+str(CPU)+" -G 0 -aS 1 -aL 1 -g 1 -i "+target+".kmer_spe.fas -o "+target+".kmer_spe.clean."+str(pclust))

#Création des séquences consensus à partir des clusters et stockage dans un fichier : 

#Tout ce with open sert à récupérer une structure de type dictionnaire, id_seq, qui contient le numéro de sonde en clé et sa séquence en valeur
with open (target+".kmer_spe.fas",'r') as f1 : 
	for l in f1 : 					#Pour chaque ligne du fichier en input : 
		l = l.strip()
		if l.startswith(">") :				#Je met le numéro de sonde en clé
			nom = l.lstrip(">")
			id_seq[nom] = ""
		else :						#Quand je vois une séquence, je l'ajoute au dictionnaire pour la dernière clé définie
			id_seq[nom] = l

with open (target+".kmer_spe.clean."+str(pclust)+".clstr",'r') as f1 : 
	for l in f1 : 
		l = l.strip()
		if l.startswith('>Cluster'): 
			if cluster_id != "" : #If a first temp.fas was create               	
				L = list()
				with open ('tmp.fas','r') as f2 : 
					for l2 in f2:
						l2 = l2.strip()
						if not l2.startswith('>'):
							L.append(Seq(l2)) #each sequence in the cluster in a list (Bio.Seq)
				motif = motifs.create(L)
				cons  = motif.degenerate_consensus         #iupac consensus from the sequences of the list			
				scoretot = 1
				os.system("rm tmp.fas")
				for aa in list(cons) :
					scoretot = scoretot * degen[aa]
			if scoretot != 0 and scoretot <= degen_score : 
				h_header[cluster_id] = cons+";"+str(nb_seq)+";"

				if not re.search('[A|M|R|W|V|H|D|I|N]{'+max_repet+'}',str(cons)) or not re.search('[T|W|Y|K|H|D|B|I|N]{'+max_repet+'}',str(cons)) or not re.search('[C|M|S|Y|V|H|B|I|N]{'+max_repet+'}',str(cons)) or not re.search('[G|R|S|K|V|D|B|I|N]{'+max_repet+'}',str(cons)) :
					with open ("tmp_blast.fas","a") as out : #Fichier contenant les séquences consensus, en fasta, numérotée en header.
						print(">"+str(compt_id), cons, sep = '\n', file = out)
			cluster_id = l.split()[1]  #Number of the cluster
			clust      = False
			compt_id  += 1
		else: 
			clust = True
		if clust :	
			#temp.fas at the end of the if = id and sequence on the current Cluster
			regexp = re.search("\d\s+\d+aa, >(\d+_\d+)",l).group(1)   #Id corresponding to the header of the fasta file with kmers
			with open ('tmp.fas', 'a') as out :
				print(">"+regexp, id_seq[regexp], sep = "\n", file = out)


##for the last file of cluster unused in the loop (rend le fichier de résultat différent de celui de la version originale de kaspod car une erreur a été faite).
L = list()
with open ('tmp.fas','r') as f2 :               #On ouvre le dernier fichier généré par la boucle précédente, qui n'est pas traité
	for l2 in f2:
		l2 = l2.strip()
		if not l2.startswith('>'):
			L.append(Seq(l2))       #each sequence in the cluster in a list (Bio.Seq)
motif = motifs.create(L)
cons  = motif.degenerate_consensus              #iupac consensus from the sequences of the list			
scoretot = 1
os.system("rm tmp.fas")			
for aa in list(cons) :			        #Parcours de tous les nucléotides de la séquence consensus pour calculer le score de dégénérescence
	scoretot = scoretot * degen[aa]
if scoretot != 0 and scoretot <= degen_score :	#Si le score est inférieur au seuil limite, on remplit tmp_blast.fas avec la nouvelle séquence consensus 
	if not re.search('[A|M|R|W|V|H|D|I|N]{'+max_repet+'}',str(cons)) or not re.search('[T|W|Y|K|H|D|B|I|N]{'+max_repet+'}',str(cons)) or not re.search('[C|M|S|Y|V|H|B|I|N]{'+max_repet+'}',str(cons)) or not re.search('[G|R|S|K|V|D|B|I|N]{'+max_repet+'}',str(cons)) :
		with open ("tmp_blast.fas","a") as out : #Fichier contenant les séquences consensus, en fasta, numérotée en header.
			print(">"+str(compt_id), cons, sep = '\n', file = out)					

#On coupe les fichiers par tranches de 5000 lignes (2500 séquences) pour diviser le travail que l'on fournit à Patman : 
os.system("split tmp_blast.fas -d -l 5000 temp/tmp_blast_")

#PATMAN revision 1.2 (~1.7s sur petit jeu)
#+ calcul de sensibilité et spécificité 

files = listdir("temp")
for fic in files : #Pour chaque fichiers dans le répertoire temp
	os.system("./patman -e "+str(edit)+" -D "+target+" -P temp/"+fic+" -o res_sen.txt -a") #each turn of for on travaille sur le out avant qu'il ne soit supprimé ensuite
	with open ('res_sen.txt','r') as f1 : 	    #Lecture du fichier de résultat :
		for l in f1 : 			      #Pour chaque ligne du fichier : 	
			l = l.split('\t')             	#On enlève les sauts de ligne à la fin des lignes (uniquement pratique)
			ID_pos[l[1]] = l[2] 		#Position de chaque sonde sur la dernière séquence cible trouvée (essayer de faire en sorte que ca soit la + grande)
			liste_cible.append(l[0])
			cible_sonde[l[0]].append(l[1])        #Toutes les sondes qui matchent sur une cible
			match_sen[int(l[1])].append(l[0])  	#On met dans un dictionnaire la séquence cible sur laquelle match la sonde sur cette ligne 	#Derrière le nom de la séquence cible que l'on vient d'ajouter à la sonde, on met ##
			deb[l[1]][l[0]]   = int(l[2])            	#Récupération de la position de début du match dans le fichier
	for k in sorted(match_sen.keys()) :           #Pour chaque valeur (ID de sondes) dans le dictionnaire remplit juste avant :
		t = set(match_sen[k])   	#t prend la valeur du dict pour la sonde courante (k) transformée en liste parsée par les ## (liste de nom de target)
		sensi[k] = round((len(t)/nb_seq*100) , 2)		#On attribue une valeur de sensibilité à chaque sondes (k) 

for fic in files : 
	os.system("./patman -e "+str(edit)+" -D "+nontarget+" -P temp/"+fic+" -o res_spe.txt -a") 
	with open ('res_spe.txt','r') as f1 : 
		for l in f1 : 
			l = l.split('\t')
			match_spe[int(l[1])] += l[0]	#Contient qui a cross hybridé avec la sonde en question
			match_spe[int(l[1])] += "##"
			seq_with_cross.add(l[1])        #Toutes les sondes qui ont des cross hybrid (outil de validation du script) 
	for k in sorted(match_spe.keys()) :
		t = (match_spe[k].split('##'))[:-1]
		t = set(t)
		speci[k] = len(t)			#On attribue une valeur de spécificité à chaque sondes (k) = au nombre de séquences target que la sonde cible

#A ce niveau, sensi et speci sont des structures remplies pour tous les fichiers présents dans temp et on peut se servir de ces dictionnaire sur le fichier tmp_blast.fas

#Récupérer sonde : couverture, trier par couverture décroissante le dictionnaire et prendre la première valeur.
#Toutes les sondes dans sensi sont celles qui nous intéressent, on élimine seulement celles qui ont une cross hybridation supérieure au seuil (tout dans speci et sensi)

sensiS = dict()
order = list()

#Avoir la sonde avec la meileure couverture globale
for sonde in sorted(sensi.items(), key=lambda x: x[1]) :
	if int(speci[sonde[0]]) <= int(maxcross) :
		best_cover = sonde[0]
order.append(best_cover) #Set de sonde minimal pour couvrir les cibles

for cible in set(liste_cible) :
	print(cible)
	for spe_sonde in set(cible_sonde[cible]) : #Pour toutes les sondes de cette cible, je crée un dict Sondes - sensibilité pour trier par couverture
		sensiS[str(spe_sonde)] = sensi[int(spe_sonde)] 
	for best_cover2 in sorted(sensiS.items(), key=lambda x : x[1], reverse=True) :
		print(best_cover2)
		nb = 0
		nb2 = 0
		for sonde_select in set(order) : 
			print(len(set(order)))
			if sonde_select in set(cible_sonde[cible]) : #Si la sonde de order match avec la cible, vériier si la sonde courante est en dehors de la fenetre: 
				nb2 += 1
				if (int(deb[str(best_cover2[0])][cible])+k_size <= int(deb[str(sonde_select)][cible]))-300 or (int(deb[str(best_cover2[0])][cible]) >= int(deb[str(sonde_select)][cible])+k_size)+300 :
					nb += 1
				
			#Si aucune sonde de order ne match avec cette cible, on met la séquence de la meilleure sonde					
			#Si la sonde ne chevauche aucune fenetre de sonde déja placées, alors : 			
		if nb == len(set(order)) :
			order.append(best_cover2[0])

		for v in order : 
			if v in cible_sonde[cible] : 
				nb2 += 1
		if nb2 == 0 : 
			pass
			#order.append(best_cover2[0])
				
print(order)		
		
		
				
#la manière de trouver les meilleures sondes et la mise en place d'un while est trop fortement chronophage : trouver un moyen de trier les séquences par couverture pour les utiliser directement dans une boucle for. Les sondes doivent être dans la cible également
#Meme sans while, le script est excessivement lent


#Voir à quel endroi le script met du temps et pq il ne se finit pas (sans doute un problème de signe < qui sont mal orientés ou de conditions de if / types des variables)

#Pour toutes les cibles je regarde les sondes qui matchent avec en définissant à chaque sonde ajouté cette zone de 300pb ou on ne peut plus mettre de sondes
#Masquer les positions de la sonde spécifiques de cette sonde dans l'autre séquence

os.system("rm tmp_blast.fas res.txt res_sen.txt res_spe.txt temporary_results temporary_results2 "+nontarget+".res "+target+".*")
os.system("rm -r temp/")			
	
'''		
Trouver la meilleure sonde globale		
Mettre la meilleure sonde dans order et créer la fenetre dans window 			
Pour chaque séquences cibles :
	npc = len(...)
	Tant que l'on n'a pas essayé d'aligner toutes les sondes à la cible courante :
		Parcourir les sondes qui matchent cette cible :
			Sélectionner la meilleure sonde (spe_sonde) en terme de couverture
		Parcourir les positions des sondes qui sont déja dans order (avec deb, permet d'adapter les positions des sondes à la cible en cours)	
			Si la sonde cible cette séquence cible : 	
				Si spe_sonde ne se trouve dans aucune des fenêtres qui sont déja dans order pour la cible :
					Ajouter spe_sonde à order
					npc -= 1
				Sinon npc -= 1
					
npc perd un point à chaque fois que l'on parcours une sonde de la cible, quand elle sont toutes parcourues, fin du script.		
deb[sonde][cible] vaut 0 quand la sonde de order ne couvre pas cette cible. 
				
Partir de la meilleure sonde globale puis prendre la première cible, positionner les sondes par ordre de couverture décroissante, changer de cible et procéder pareil avec ses sondes en tenant compte de celles qui sont déja positionnées. 	
'''	
	
	
	
